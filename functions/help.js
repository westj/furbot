const fs = require('fs')

function helpFunction(issue) {
    console.log(issue)
    const helps = JSON.parse(fs.readFileSync("./helpTopics.json"))
    if(issue) {
        return helps[issue]
    } else {
        return Object.keys(helps)
    }
}

module.exports.helpFunction = helpFunction;