const Discord = require('discord.js');
const client = new Discord.Client();
const {helpFunction} = require("./functions/help")

client.on('message', message => {
    if(message.content.substr(0,5) === "!help") {
        // throw the request to an external function
        const help = helpFunction(message.content.substr(6))

        let title, content;

        if (typeof help === "undefined") {
            title = "Command not found"
            content = "Simply type \"!help\" for commands"
        }
        else if (typeof help === "array") {
            title = "Help Topics"

            content = ""
            help.forEach(key => {
                content = `\`${key}\` `
            })
        } else {
            title = message.content.substr(6)
            content = `${help.purpose} \n**Scope:** ${help.scope} \n**Usage:** ${help.usage} \n**Permissions:** ${help.permissions} `
        }

        const embed = new Discord.RichEmbed();

        embed.setTitle(title)
        embed.setDescription(content)

        message.channel.send(embed)
        
    }
    if(message.content.includes('ping')) {
        message.channel.send('pong!')
    }
});

client.login(process.env.TOKEN);